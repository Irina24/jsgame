from flask import Flask, redirect, request

app = Flask(__name__)


@app.route("/")
def index():
    return redirect("/index.html")


@app.route("/help")
def indexhtml():
    return redirect("/help.html")


@app.route("/about")
def abouthtml():
    return redirect("/about.html")


@app.route("/game")
def gamehtml():
    if request.environ.get('wsgi.websocket'):
        websocket = request.environ['wsgi.websocket']
        # Do the waiting room magic
        return "Ok"
    else:
        return redirect("/game.html")


if app.config['DEBUG']:
    from werkzeug import SharedDataMiddleware
    import os
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
      '/': os.path.join(os.path.dirname(__file__), 'static')
    })

if __name__ == '__main__':
    app.run(DEBUG=True)
