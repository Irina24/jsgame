Requirements
============

    * Linux (like Ubuntu)
    * Git
    * Python (gevent, greenlet, gunicorn, flask)
    * PyBox2D 2.1b

Installation
============
_(for ubuntu like systems)_

        sudo apt-get install git python python-gevent python-greenlet python-pip python-flask 
        sudo pip install gunicorn gevent-websocket

PyBox2D installation:
    
    http://code.google.com/p/pybox2d/wiki/BuildingfromSource

How-to run:
===========

0. Install prerequirements and git

1. With your's username run this command:
        
        git clone https://Irina24@bitbucket.org/Irina24/jsgame.git irina

2. Run frontend

        ./frontend.sh

3. Go to http://localhost:8000/ to start playing

Documentation
=============

Summary docs: http://airlab.math.usu.ru/redmine/projects/jsgame/wiki

Box2Dweb: http://www.box2dflash.org/docs/2.1a/reference/

Errors fix
----------

If gunicorn falls like "bla bla bla **Logger** bla bla bla":
http://stackoverflow.com/questions/9444405/gunicorn-and-websockets

Usefull links
-------------

Echo server: https://gist.github.com/1185629
