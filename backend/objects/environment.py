import uuid
import random


class Barrel:
    @classmethod
    def Data(cls):
        return {
            "id": str(uuid.uuid1()),
            "object": "Barrel",
            "type": "dynamic",
            "polygones": [
                [-5, -5],
                [5, -5],
                [5, 5],
                [5, -5]
            ]
        }

    @classmethod
    def create(cls, world):
        body = world.CreateDynamicBody(
            position=(random.random() * 800, random.random() * 600),
            userData=Barrel.Data()
        )
        body.CreatePolygonFixture(
            box=(1, 1),
            density=1,
            friction=0.3
        )
        return body
