from flask import Flask, redirect, request

app = Flask(__name__)


@app.route("/")
def index():
    if request.environ.get('wsgi.websocket'):
        websocket = request.environ['wsgi.websocket']
        # Do the game magic
        return "Ok"
    else:
        return redirect("/index.html")


if app.config['DEBUG']:
    from werkzeug import SharedDataMiddleware
    import os
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
      '/': os.path.join(os.path.dirname(__file__), 'static')
    })

if __name__ == '__main__':
    app.run(DEBUG=True)
